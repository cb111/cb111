#include <stdio.h>

int main()
{
    int num1, num2, sum, subtract, mult, div, rem;
    int *ptr1, *ptr2;

    ptr1 = &num1; // ptr1 stores the address of num1
    ptr2 = &num2; // ptr2 stores the address of num2

    printf("Enter any two numbers: ");
    scanf("%d%d", ptr1, ptr2);

    sum = *ptr1 + *ptr2;
    printf("Sum = %d\n", sum);
    subtract=*ptr1 - *ptr2;
    printf("subtraction = %d\n", subtract);
    mult= *ptr1 * *ptr2;
    printf("product =%d\n",mult);
    div = *ptr1 / *ptr2;
    printf("dividend= %d\n", div);
    rem= *ptr1 % *ptr2 ;
    printf("remainder= %d\n", rem);
    return 0;
    
 }   
    
