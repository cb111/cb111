

#include <stdio.h>
int main() {
    int a[10][10], transpose[10][10], ro, col, i, j;
    printf("Enter rows and columns: ");
    scanf("%d %d", &ro, &col);
printf("\nEnter matrix elements:\n");
    for (i = 0; i < ro; ++i)
        for (j = 0; j < col; ++j) {
            printf("Enter element a%d%d: ", i + 1, j + 1);
            scanf("%d", &a[i][j]);
        }

    printf("\nEntered matrix: \n");
    for (i = 0; i < ro; ++i)
        for (j = 0; j < col; ++j) {
            printf("%d  ", a[i][j]);
            if (j == col - 1)
                printf("\n");
        }

    for (i = 0; i < ro; ++i)
        for (j = 0; j < col; ++j) {
            transpose[j][i] = a[i][j];
        }


printf("\nTranspose of the matrix:\n");
    for (i = 0; i < col; ++i)
        for (j = 0; j < ro; ++j) {
            printf("%d  ", transpose[i][j]);
            if (j == ro - 1)
                printf("\n");
        }
    return 0;
}